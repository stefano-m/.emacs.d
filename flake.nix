{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";

  outputs = { self, nixpkgs }@inputs:
    let
      versionSuffix = if (self ? "shortRev") then "" else "-dirty";

      version = builtins.readFile ./VERSION + versionSuffix;

      # taken from github:NixOS/nix/flake.nix
      systems = [ "x86_64-linux" "x86_64-darwin" ];
      forAllSystems = f: nixpkgs.lib.genAttrs systems (system: f system);
      nixpkgsFor = forAllSystems (system:
        import nixpkgs {
          inherit system;
          overlays = [ self.overlays.default ];
        }
      );

    in
    {
      overlays.default = final: prev: {
        stefano-m.emacsen = self.packages.${final.system};
      };

      packages = forAllSystems (system:
        let
          flakePkgs = nixpkgsFor.${system};
        in
        rec {
          default = flakePkgs.callPackage ./default.nix {
            inherit version;
            pkgs = flakePkgs;
          };

          full = default.override {
            extraConfigs = "all";
          };

          python-ide = default.override {
            extraConfigs = [ "python" ];
          };

          clojure-ide = default.override {
            extraConfigs = [ "clojure" ];
          };

          lua-ide = default.override {
            extraConfigs = [ "lua" ];
          };

          devops-ide = default.override {
            extraConfigs = [ "javascript" "yaml" "terraform" "docker" ];
          };

          golang-ide = default.override {
            extraConfigs = [ "golang" ];
          };

        });

      apps = forAllSystems (system:
        let
          flakePkgs = nixpkgsFor.${system};
        in
        with flakePkgs.lib.attrsets;
        mapAttrs (name: value: { type = "app"; program = "${value}/bin/emacs"; }) self.packages.${system}
      );

      devShells = forAllSystems (system:
        let
          flakePkgs = nixpkgsFor.${system};
        in
        {
          default = flakePkgs.mkShell {
            buildInputs = with flakePkgs; [
              nixpkgs-fmt
            ];
          };
        });

    };

}
