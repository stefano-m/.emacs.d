# buildEnv will call make with the default argument, so leave VERSION as first
# target to avoid errors.
VERSION:
	git describe | cut -d'-' -f1 | tr -d 'v\n' > VERSION

.PHONY: build
build: VERSION
	nix build

.PHONY: build-full
build-full: VERSION
	nix build '.#full'

.PHONY: clean
clean:
	-rm -f VERSION result
