#!/usr/bin/env bash
find . -name elpa -prune -o -name .git -prune -o -type f -name "*.elc" -exec rm -f '{}' \; -print
