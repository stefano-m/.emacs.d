;;; buffer.cfg.el --- Summary
;;; Commentary:
;;; configuration for buffer-related stuff.
;;; Code:

(use-package comint
  :functions my-shell-turn-echo-off View-quit
  :defines comint-process-echoes universal-argument-num-events
  :init
  (defun my-shell-turn-echo-off ()
    "Do not echo stuff in shell-mode."
    (setq comint-process-echoes t))
  (add-hook 'shell-mode-hook #'my-shell-turn-echo-off)
  :bind (:map comint-mode-map
              ("C-c C-l" . helm-comint-input-ring)))

(use-package compile
  :defines compilation-scroll-output
  :init
  (setq compilation-scroll-output t))

;; Case-insensitive search
(setq case-fold-search t)

(setq current-language-environment "UTF-8")
(setq default-input-method "utf-8")
(prefer-coding-system 'utf-8)
(when (display-graphic-p)
  (setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING)))

(use-package dired
  :config
  ;; Don't spawn a new buffer for every directory I visit.
  (put 'dired-find-alternate-file 'disabled nil)
  :bind (:map dired-mode-map
              ("RET" . dired-find-alternate-file)))

(use-package smartscan
  :config
  (global-smartscan-mode 1))

(use-package saveplace
  :defines save-place-file
  :config
  (setq-default save-place t)
  (setq save-place-file (concat user-emacs-directory "saved-places")))

;; Same-frame speedbar
(use-package sr-speedbar
  :init
  ;; Use Semantic with Speedbar
  (add-hook 'speedbar-load-hook #'(lambda () (require 'semantic/sb)))
  ;; Win-S
  :bind ("s-S" . sr-speedbar-toggle))

(global-set-key (kbd "RET") 'newline-and-indent)

(setq require-final-newline t)

;; Start from the top when usint C-l
(setq recenter-positions '(top middle bottom))

;; use shift to move around windows
(windmove-default-keybindings 'shift)
;; Window splitting and navigation.
(winner-mode 1)

(use-package windresize
  :bind ("C-c r" . windresize))

(use-package popwin
  :functions popwin-mode
  :config
  (popwin-mode 1))

;; Make unique buffer names better.
;; That's the default in emacs 24.4 and above.
(use-package uniquify
  :if (string< emacs-version "24.4")
  :config
  (setq uniquify-buffer-name-style 'post-forward-angle-brackets))

;; http://www.masteringemacs.org/articles/2014/02/28/my-emacs-keybindings/
(defun kill-this-buffer ()
  "Kill the current buffer."
  (interactive)
  (kill-buffer (current-buffer)))
(global-set-key "\C-x\C-k" 'kill-this-buffer)

(defun revert-this-buffer ()
  "Revert the current buffer."
  (interactive)
  (revert-buffer nil t t)
  (message (concat "Reverted buffer " (buffer-name))))
(global-set-key [f6] 'revert-this-buffer)

;; http://www.emacswiki.org/emacs/FlySpell
;; Printing messages for every word (when checking the entire buffer) causes an enormous slowdown.
(use-package flyspell
  :config
  (setq flyspell-issue-message-flag nil)
  :delight flyspell-mode)

(use-package ispell
  :if (executable-find "aspell")
  :config
  (when (boundp 'ispell-list-command)
    (setq
     ;; http://www.emacswiki.org/emacs/FlySpell
     ;; Use --list instead of -l because the -l option means --lang in aspell
     ispell-list-command "--list"))
  (setq ispell-program-name "aspell"
        ;; http://blog.binchen.org/posts/effective-spell-check-in-emacs.html
        ;; force the English dictionary, support Camel Case spelling check (--run-together)
        ispell-extra-args '("--sug-mode=ultra" "--lang=en_US" "--run-together" "--run-together-limit=5" "--run-together-min=2")))

(use-package hideshow
  :delight hs-minor-mode)

(use-package drag-stuff
  :defines drag-stuff-mode-map
  ;; Explicitly define keys as drag-stuff ones don't seem to work.
  :bind (:map drag-stuff-mode-map
              ("<M-S-up>" . drag-stuff-up)
              ("<M-S-down>" . drag-stuff-down)
              ("<M-S-left>" . drag-stuff-left)
              ("<M-S-right>" . drag-stuff-right))
  :delight drag-stuff-mode)
(drag-stuff-global-mode t)

(use-package clean-aindent-mode
  :init
  (add-hook 'prog-mode-hook #'clean-aindent-mode))

;; unobtrusively trim white spaces from end of line
(use-package ws-butler
  :init
  (add-hook 'prog-mode-hook #'ws-butler-mode)
  :delight ws-butler-mode)

;; improved undo system
(use-package undo-tree
  :config
  (global-undo-tree-mode)
  :delight undo-tree-mode)

;; Works best when Editorconfig C Core is installed
(use-package editorconfig
  :config
  (editorconfig-mode 1)
  :delight editorconfig-mode)

;; Better navigation for CamelCase words
(use-package subword
  :hook ((java-mode kotlin-mode go-mode) . subword-mode))

;; AutoType Skeletons
;; They can be called like a function: M-x <skeleton name> in a buffer
;; See http://www.gnu.org/software/emacs/manual/html_mono/autotype.html

(define-skeleton my/skeletons/elisp-package
  "Skeleton to Elisp packages"
  > ";;; " (buffer-name) " --- Summary\n"
  > ";;; Commentary:\n"
  > ";;; " _ "\n"
  > ";;; Code:\n"
  >  "\n"
  > "(provide '" (substring (buffer-name) 0 -3) ")\n"
  > ";;; " (buffer-name) " ends here\n")

(use-package direnv :config (direnv-mode))

(provide 'buffer.cfg)
;;; buffer.cfg.el ends here
