;;; cc.cfg.el --- Summary
;;; Commentary:
;;; configuration for C languages
;;; Code:
(use-package cc-mode
  :init
  (setq c-default-style "stroustrup")
  (add-hook 'c-mode-common-hook #'hs-minor-mode)
  (add-hook 'c-mode-common-hook #'flycheck-mode)
  :config
  (sp-with-modes '(c++-mode)
    (sp-local-pair "{" nil :post-handlers '(("||\n[i]" "RET"))))
  (sp-local-pair 'c++-mode "/*" "*/" :post-handlers '((" | " "SPC")
                                                      ("* ||\n[i]" "RET")))
  :mode ("\\.h\\'" . c++-mode)
  :bind (("C-c f" . ff-find-other-file)))

(provide 'cc.cfg)
;;; cc.cfg.el ends here
