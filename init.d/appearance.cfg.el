;;; appearance.cfg.el --- Summary
;;; Commentary:
;;; appearance configuration
;;; Code:
(global-font-lock-mode t)
(set-default 'show-trailing-whitespace t)
(global-set-key (kbd "C-c w") 'whitespace-mode)
;; fill-column influences how Emacs justifies paragraphs
(setq-default fill-column 79)
;; show both line and column number by default
(setq column-number-mode t)
(setq line-number-mode t)
;; spaces instead of tabs
(setq-default indent-tabs-mode nil)
(setq tab-width 4)
;; Turn beep off
(setq visible-bell nil)
;; Ask for 'y or n'
(fset 'yes-or-no-p 'y-or-n-p)

;; Set "<file name> [<project name>]" as frame title when available
;; Ref: https://emacs-fu.blogspot.com/2011/01/setting-frame-title.html
(setq frame-title-format
      '(""
        (:eval (if-let* ((buf-f-name (buffer-file-name)))
                   (abbreviate-file-name buf-f-name)
                 "%b"))
        (:eval (let* ((pname (projectile-project-name)))
                 (unless (string-equal "-" pname)
                   (format " [%s]" pname))))))

;; Don't open a dedicated frame for ediff even in X but
;; place it at the bottom.
(use-package ediff-wind
  :defines ediff-split-window-function ediff-window-setup-function
  :config
  (setq ediff-split-window-function 'split-window-horizontally
        ediff-window-setup-function 'ediff-setup-windows-plain))

(use-package ansi-color
  :config
  (defun my/colorize-buffer ()
    "Colorize the current buffer.  Useful when opening a buffer
that has ANSI escape sequences.  All escape sequences will be
filtered out from the file!"
    (interactive)
    (with-current-buffer (current-buffer)
      (ansi-color-apply-on-region (point-min) (point-max)))))

(use-package compile
  :functions endless/colorize-compilation ansi-color-apply-on-region
  :config
  ;; http://endlessparentheses.com/ansi-colors-in-the-compilation-buffer-output.html
  (defun endless/colorize-compilation ()
    "Colorize from `compilation-filter-start' to `point'."
    (let ((inhibit-read-only t))
      (ansi-color-apply-on-region
       compilation-filter-start (point))))
  (add-hook 'compilation-filter-hook #'endless/colorize-compilation))

(provide 'appearance.cfg)
;;; appearance.cfg.el ends here
