;;; autocomplete.cfg.el --- Summary
;;; Commentary:
;;; auto completion configuration
;;; Code:

(use-package lsp-mode
  :commands lsp
  :functions lsp-enable-imenu
  :config
  (use-package lsp-ui
    :commands lsp-ui-mode
    :config (lsp-ui-mode))
  (setq lsp-prefer-flymake nil ;; prefer flycheck.
        lsp-auto-guess-root t) ;; guess project root with projectile.
  (lsp-enable-imenu)
  (setq
   lsp-pylsp-plugins-autopep8-enabled nil
   lsp-pylsp-plugins-flake8-enabled t
   lsp-pylsp-plugins-jedi-completion-enabled t
   lsp-pylsp-plugins-jedi-definition-enabled t
   lsp-pylsp-plugins-jedi-hover-enabled t
   lsp-pylsp-plugins-jedi-references-enabled t
   lsp-pylsp-plugins-jedi-signature-help-enabled t
   lsp-pylsp-plugins-jedi-symbols-enabled t
   lsp-pylsp-plugins-mccabe-enabled nil
   lsp-pylsp-plugins-preload-enabled t
   lsp-pylsp-plugins-pycodestyle-enabled nil
   lsp-pylsp-plugins-pydocstyle-enabled nil
   lsp-pylsp-plugins-pyflakes-enabled nil
   lsp-pylsp-plugins-pylint-enabled nil
   lsp-pylsp-plugins-rope-completion-enabled nil
   lsp-pylsp-plugins-yapf-enabled t
   lsp-pylsp-rename-backend 'jedi       ; alternatively use 'rope
   lsp-pylsp-rope-rope-folder ".ropeproject")
  (lsp-register-custom-settings
   ;; https://github.com/python-lsp/pylsp-mypy#configuration
   ;; https://github.com/emacs-lsp/lsp-mode/pull/1317#issuecomment-728883920
   ;; pylsp and mypy_ls are maintained forks of pyls and pyls_mypy.
   '(
     ("pylsp.plugins.pylsp_mypy.enabled" t t)
     ("pylsp.plugins.pylsp_mypy.dmypy" nil t) ;; incompatible with live_mode
     ("pylsp.plugins.pylsp_mypy.live_mode" t t)))
  (add-hook 'lsp-mode-hook
            ;; Do not install anything automatically.
            #'(lambda () (mapc (lambda (client) (setf (lsp-client-download-server-fn client) nil))
                               (ht-values lsp-clients))))
  :delight lsp-mode)

(use-package company
  :config
  (add-hook 'after-init-hook #'global-company-mode)

  (use-package company-quickhelp
    :config (company-quickhelp-mode))

  (setq company-tooltip-align-annotations t ; Align annotations to the right hand side
        company-dabbrev-downcase nil        ; Do not downcase candidates
        company-minimum-prefix-length 1     ; LSP optimization
        company-idle-delay 0.0              ; LSP optimization
        )

  :delight company-mode
  :bind ("M-SPC" . company-complete))

(use-package helm-company
  :config
  (progn
    (define-key company-mode-map (kbd "C-.") 'helm-company)
    (define-key company-active-map (kbd "C-.") 'helm-company)))

(provide 'autocomplete.cfg)
;;; autocomplete.cfg.el ends here
