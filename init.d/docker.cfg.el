;;; docker.cfg.el --- Summary
;;; Commentary:
;;; Configuration to work with docker
;;; Code:

(use-package dockerfile-mode
  :config
  (put 'dockerfile-image-name 'safe-local-variable #'stringp)
  :mode "Dockerfile\\'")

(provide 'docker.cfg)
;;; docker.cfg.el ends here
