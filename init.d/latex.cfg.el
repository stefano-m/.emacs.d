;;; latex.cfg.el --- Summary
;;; Commentary:
;;; LaTeX configuration
;;; Code:

(use-package tex-mode
  :init
  (add-hook 'latex-mode-hook #'flyspell-mode)
  (add-hook 'latex-mode-hook #'turn-on-auto-fill)
  (add-hook 'latex-mode-hook #'auto-fill-mode))

(provide 'latex.cfg)
;;; latex.cfg.el ends here
