;;; vcs.cfg.el --- Summary
;;; Commentary:
;;; configuration for version control systems
;;; Code:

(use-package magit
  :defines magit-last-seen-setup-instructions
  :init
  (setq magit-last-seen-setup-instructions "1.4.0")
  :config
  (setq vc-handled-backends (delq 'Git vc-handled-backends)) ; disable default VCS
  :hook ((after-save . magit-after-save-refresh-status)
         (git-commit-mode . flyspell-mode))
  :bind (("C-x g" . magit-status)))

(use-package gitignore-mode)

(use-package gitconfig-mode)

(use-package diff-hl
  :functions diff-hl-magit-post-refresh
  :config
  (global-diff-hl-mode)
  (add-hook 'magit-post-refresh-hook #'diff-hl-magit-post-refresh))

(provide 'vcs.cfg)
;;; vcs.cfg.el ends here
