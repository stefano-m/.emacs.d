;;; yaml.cfg.el --- Summary
;;; Commentary:
;;; YAML configuration
;;; Code:
(use-package yaml-mode
  :config
  (add-hook 'yaml-mode-hook #'yas-minor-mode)
  (add-hook 'yaml-mode-hook #'flycheck-mode)

  (define-derived-mode cfn-yaml-mode yaml-mode
    "CFN-YAML"
    "Simple mode to edit CloudFormation template in YAML format.
    See `https://www.emacswiki.org/emacs/CfnLint' for more
    information.")
  (add-to-list 'magic-mode-alist
               '("\\(---\n\\)?AWSTemplateFormatVersion:" . cfn-yaml-mode))
  (add-hook 'cfn-yaml-mode-hook #'flycheck-mode))

(use-package ansible
  :functions yaml-mode ansible-doc-mode ansible-doc ansible
  :mode ("\\.yml$" . my-ansible-minor-mode)
  :init
  (defun my-ansible-minor-mode ()
    "Activate ansible minor mode on top of yaml."
    (progn
      (yaml-mode)
      (ansible t)
      (ansible-doc-mode)))
  :config
  (use-package ansible-doc
    :defines ansible-doc-mode-map
    :init
    :bind (:map ansible-doc-mode-map
                ("C-c ?" . nil)
                ("M-?" . ansible-doc))
    :delight ansible-doc-mode)
  :delight ansible)

(provide 'yaml.cfg)
;;; yaml.cfg.el ends here
