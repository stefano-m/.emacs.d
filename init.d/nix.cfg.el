;;; nix.cfg.el --- Summary
;;; Commentary:
;;; Nix and NixOS configuration
;;; Code:

(use-package nix-prettify-mode
  :hook shell-mode
  :delight)

(use-package nix-mode
  :mode "\\.nix\\'"
  :bind (:map nix-mode-map ([tab] . nix-indent-line))
  :init
  (use-package nixpkgs-fmt
    :delight nixpkgs-fmt-on-save-mode)
  (use-package flycheck
    :config
    (flycheck-define-checker nix-instantiate
      "Nix checker using nix-instantiate.
Works with nix-instantiate 2.18
See URL `https://nixos.org/nix/manual/#sec-nix-instantiate'."
      :command ("nix-instantiate" "--parse" "-")
      :standard-input t
      :error-patterns
      ((error
        line-start "error: " (message) (one-or-more "\n") line-end
        (one-or-more "\n")
        line-start (one-or-more space) "at " (one-or-more graph) "stdin" (one-or-more graph) ":" line ":" column ":" line-end))
      :error-filter
      (lambda (errors)
        (flycheck-sanitize-errors
         (flycheck-remove-error-file-names "(string)" errors)))
      :next-checkers ((warning . nix-linter))
      :modes nix-mode)
    (add-to-list 'flycheck-checkers 'nix-instantiate))
  (add-hook 'nix-mode-hook #'nixpkgs-fmt-on-save-mode)
  (add-hook 'nix-mode-hook #'flycheck-mode)
  :delight)

(use-package nix-drv-mode
  :mode "\\.drv\\'")

(use-package nix-shell
  :commands (nix-shell-unpack nix-shell-configure nix-shell-build))

(use-package nix-repl
  :commands (nix-repl))

(provide 'nix.cfg)
;;; nix.cfg ends here
