;;; terraform.cfg.el --- Summary
;;; Commentary:
;;; Terraform (https://www.terraform.io) configuration
;;; Code:

(use-package terraform-mode
  :defines company-backends
  :config
  (use-package company-terraform
    :config
    (add-to-list 'company-backends 'company-terraform))
  (add-hook
   'terraform-mode-hook
   #'terraform-format-on-save-mode))

(provide 'terraform.cfg)
;;; terraform.cfg.el ends here
