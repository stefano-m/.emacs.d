;;; lua.cfg.el --- Summary
;;; Commentary:
;;; lua configuration
;;; Code:

(use-package lua-mode
  :defines lua-mode-map lua-indent-level flycheck-lua-luacheck-executable flycheck-luacheck-standards
  :functions
  flycheck-set-checker-executable
  my-lua-flycheck-mode
  run-lua
  :init
  (defun my-lua-flycheck-mode ()
    (let* ((fname (file-name-nondirectory (buffer-file-name))) (fext (file-name-extension fname)))
      (cond ((or
              (string-equal "rockspec" fext)
              (cl-remove-if-not (lambda (x) (string-equal fname x)) '("config.ld" ".luacheckrc" ".luacov")))
             (flycheck-select-checker 'lua))
            ((string-match ".*_spec\\.lua" fname)
             (setq flycheck-luacheck-standards '("+busted")))))
    (flycheck-mode 1))

  :config
  (use-package company-lua
    :config
    (add-to-list 'company-backends 'company-lua))
  (setq lua-indent-level 2)
  (add-hook 'lua-mode-hook #'my-lua-flycheck-mode)
  :bind (:map lua-mode-map
              ("C-c l" . run-lua))
  :interpreter "lua"
  :mode ("\\.lua\\'" "\\.rockspec\\'" "config\\.ld" "\\.luacheckrc" "\\.luacov"))

(provide 'lua.cfg)
;;; lua.cfg.el ends here
