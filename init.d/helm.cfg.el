;;; helm.cfg.el --- Summary
;;; Commentary:
;;; configuration for helm
;;; Code:

;; Emacs incremental completion and selection narrowing framework
;; https://github.com/emacs-helm/helm

(use-package helm-projectile
  :config
  ;; So one can select a file in the project directly.
  (setq projectile-switch-project-action 'helm-projectile))

(use-package helm-autoloads
  :bind (([remap execute-extended-command] . 'helm-M-x)
         ([remap yank-pop] . 'helm-show-kill-ring)
         ("C-h SPC" . 'helm-all-mark-rings)
         ([remap find-file] . 'helm-find-files)
         ([remap list-buffers] . 'helm-buffers-list)
         ([remap switch-to-buffer] . 'helm-mini)
         ([remap dabbrev-expand] . 'helm-dabbrev)
         ([remap apropos-command] . 'helm-apropos)
         ([remap occur] . 'helm-occur)
         :map shell-mode-map
         ("C-c C-l" . 'helm-comint-input-ring)
         :map minibuffer-local-map
         ("C-c C-l" . 'helm-minibuffer-history)
         :map helm-command-map
         ("o" . 'helm-occur)
         ("M-s o" . nil))

  :config
  (helm-mode 1)
  (helm-adaptive-mode 1))

(use-package helm
  :defines
  helm-ff-ido-style-backspace
  helm-ff-file-name-history-use-recentf
  helm-ff-newfile-prompt-p
  helm-ff-skip-boring-files
  helm-ff-auto-update-initial-value
  helm-ff--auto-update-state

  :delight helm-mode
  :bind (:map helm-map
              ([left] . helm-previous-source)
              ([right] . helm-next-source))
  :config
  (require 'helm-autoloads)
  (setq helm-move-to-line-cycle-in-source t
        helm-ff-file-name-history-use-recentf t
        helm-ff-newfile-prompt-p nil
        helm-ff-skip-boring-files t
        helm-ff-ido-style-backspace 'always
        helm-ff-auto-update-initial-value t
        helm-ff--auto-update-state t))

(use-package helm-files
  :bind ([f7] . helm-browse-project)
  :custom
  (helm-ff-lynx-style-map t))

(use-package helm-imenu
  :custom
  (helm-imenu-lynx-style-map t))

(use-package helm-occur
  :custom
  (helm-occur-use-ioccur-style-keys t))

(use-package helm-c-yasnippet

  :config
  (setq helm-yas-space-match-any-greedy t
        helm-yas-display-msg-after-complete nil
        helm-yas-display-key-on-candidate t)
  :bind
  ("C-c y" . helm-yas-complete))

(when (executable-find "global")
  (use-package helm-gtags

    :init
    (add-hook 'dired-mode-hook #'helm-gtags-mode)
    (add-hook 'eshell-mode-hook #'helm-gtags-mode)
    (add-hook 'c-mode-hook #'helm-gtags-mode)
    (add-hook 'c++-mode-hook #'helm-gtags-mode)
    (add-hook 'asm-mode-hook #'helm-gtags-mode)
    :config
    (setq
     helm-gtags-ignore-case t
     helm-gtags-auto-update t
     helm-gtags-use-input-at-cursor t
     helm-gtags-pulse-at-cursor t
     helm-gtags-prefix-key "\C-cg"
     helm-gtags-suggested-key-mapping t)
    (define-key helm-gtags-mode-map (kbd "C-c g a") 'helm-gtags-tags-in-this-function)
    (define-key helm-gtags-mode-map (kbd "C-j") 'helm-gtags-select)
    (define-key helm-gtags-mode-map (kbd "M-.") 'helm-gtags-dwim)
    (define-key helm-gtags-mode-map (kbd "M-,") 'helm-gtags-pop-stack)
    (define-key helm-gtags-mode-map (kbd "C-c <") 'helm-gtags-previous-history)
    (define-key helm-gtags-mode-map (kbd "C-c >") 'helm-gtags-next-history)
    :delight helm-gtags-mode))

(when (executable-find "ag")
  (use-package helm-ag))

(when (executable-find "git")
  (use-package helm-ls-git))

(when (executable-find "hg")
  (use-package helm-ls-hg))

(provide 'helm.cfg)
;;; helm.cfg.el ends here
