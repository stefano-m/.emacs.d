;;; org-mode.cfg.el --- Summary
;;; Commentary:
;;; org-mode configuration
;;; Code:

(use-package org
  :functions org-update-statistics-cookies
  :init
  (add-hook 'org-mode-hook #'visual-line-mode)
  (add-hook 'org-mode-hook #'variable-pitch-mode)
  (add-hook 'org-mode-hook #'flyspell-mode)
  (add-hook 'org-mode-hook #'auto-fill-mode)
  (add-hook 'org-mode-hook #'ws-butler-mode)
  (add-hook 'org-mode-hook
            #'(lambda ()
                ;; Register " as verbatim
                ;; http://permalink.gmane.org/gmane.emacs.orgmode/82669
                (setcar (nthcdr 2 org-emphasis-regexp-components) " \t\n,'")
                (custom-set-variables `(org-emphasis-alist ', org-emphasis-alist))))

  (add-hook 'org-mode-hook
            ;; https://stackoverflow.com/questions/6138029/how-to-add-a-hook-to-only-run-in-a-particular-mode#6141681
            #'(lambda ()
                (add-hook 'before-save-hook
                          ;; update checkboxes
                          #'(lambda () (org-update-statistics-cookies t)) t t)))

  :config
  (use-package org-tempo) ;; https://orgmode.org/manual/Structure-Templates.html
  (use-package org-tree-slide
    :init
    (add-hook 'org-tree-slide-play-hook
              ;; Hide ord-meta-line i.e. #+
              ;; Hide emphasis markup i.e. /.../ for italics, *...* for bold, etc.
              #'(lambda ()
                  (interactive)
                  (progn
                    (setq org-hide-emphasis-markers t)
                    (set-face-attribute 'org-meta-line nil
        		                :foreground (face-attribute 'default :background))
                    (font-lock-update))))
    (add-hook 'org-tree-slide-stop-hook #'(lambda ()
                                            (interactive)
                                            (progn
                                              (setq org-hide-emphasis-markers nil)
                                              (set-face-attribute 'org-meta-line nil
                                                                  :foreground nil)
                                              (font-lock-update))))
    (setq org-tree-slide-skip-outline-level 4)
    :custom
    (org-image-actual-width nil "Get the width from an #+ATTR.* keyword")
    :config
    (use-package org-tree-slide-pauses))
  ;; https://zzamboni.org/post/beautifying-org-mode-in-emacs/
  (font-lock-add-keywords 'org-mode
                          '(("^ *\\([-]\\) " ; replace list symbol form - to •
                             (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))
  (let* ((font
          (cond ((x-list-fonts "ETBembo")         '(:font "ETBembo"))
                ((x-list-fonts "Source Sans Pro") '(:font "Source Sans Pro"))
                ((x-list-fonts "Lucida Grande")   '(:font "Lucida Grande"))
                ((x-list-fonts "Verdana")         '(:font "Verdana"))
                ((x-family-fonts "Sans Serif")    '(:family "Sans Serif"))
                (nil (warn "Cannot find a Sans Serif font."))))
         (headline           `(:inherit default :weight bold)))

    (custom-theme-set-faces
     'user
     `(variable-pitch ((t (,@headline ,@font :height 180 :weight thin))))
     ;; foreground colors base on solarized-wombat-dark theme
     `(org-level-8 ((t (,@headline ,@font))))
     `(org-level-7 ((t (,@headline ,@font))))
     `(org-level-6 ((t (,@headline ,@font))))
     `(org-level-5 ((t (,@headline ,@font))))
     `(org-level-4 ((t (,@headline ,@font :height 1.1))))
     `(org-level-3 ((t (,@headline ,@font :height 1.25))))
     `(org-level-2 ((t (,@headline ,@font :height 1.5))))
     `(org-level-1 ((t (,@headline ,@font :height 1.75))))
     `(org-document-title ((t (,@headline ,@font :height 2.0 :underline nil :weight bold))))))

  (custom-theme-set-faces
   'user
   '(org-block ((t (:inherit fixed-pitch))))
   '(org-code ((t (:inherit (shadow fixed-pitch)))))
   '(org-document-info ((t (:foreground "dark orange"))))
   '(org-document-info-keyword ((t (:inherit (shadow fixed-pitch)))))
   '(org-indent ((t (:inherit (org-hide fixed-pitch)))))
   '(org-link ((t (:foreground "royal blue" :underline t))))
   '(org-meta-line ((t (:inherit (font-lock-comment-face fixed-pitch)))))
   '(org-property-value ((t (:inherit fixed-pitch))) t)
   '(org-special-keyword ((t (:inherit (font-lock-comment-face fixed-pitch)))))
   '(org-table ((t (:inherit fixed-pitch :foreground "#83a598"))))
   '(org-tag ((t (:inherit (shadow fixed-pitch) :weight bold :height 0.8))))
   '(org-verbatim ((t (:inherit (shadow fixed-pitch))))))

  ;; http://www.farseer.cn/tweak/2014/11/10/org-your-notes/
  (setq org-directory "~/docs/org"
        org-enforce-todo-dependencies t
        org-default-notes-file "~/docs/org/notes.org"
        org-agenda-files `(,(format "%s/agenda" org-directory))
        org-adapt-indentation nil
        org-indent-mode-turns-on-hiding-stars nil
        org-startup-indented t)
  (unless (file-exists-p org-directory)
    (make-directory org-directory t))
  (setq org-log-done nil)
  (setq org-file-apps '((auto-mode . emacs24)
                        ("\\.mp4\\'" . "xdg-open %s")
                        ("\\.pdf\\'" . "xdg-open %s")))
  (setq org-startup-with-latex-preview nil)
  (setq org-startup-with-inline-images nil)

  (defun sp--org-skip-asterisk (ms mb me)
    (or (and (= (line-beginning-position) mb)
             (eq 32 (char-after (1+ mb))))
        (and (= (1+ (line-beginning-position)) me)
             (eq 32 (char-after me)))))
  (sp-with-modes 'org-mode
    (sp-local-pair "*" "*"
                   :actions '(insert wrap)
                   :unless '(sp-point-after-word-p sp-point-at-bol-p)
                   :wrap "C-*"
                   :skip-match 'sp--org-skip-asterisk)
    (sp-local-pair "_" "_"
                   :unless '(sp-point-after-word-p)
                   :wrap "C-_")
    (sp-local-pair "/" "/"
                   :unless '(sp-point-after-word-p)
                   :post-handlers '(("[d1]" "SPC")))
    (sp-local-pair "~" "~"
                   :unless '(sp-point-after-word-p)
                   :post-handlers '(("[d1]" "SPC")))
    (sp-local-pair "=" "="
                   :unless '(sp-point-after-word-p)
                   :post-handlers '(("[d1]" "SPC")))
    (sp-local-pair "«" "»")))

(provide 'org-mode.cfg)
;;; org-mode.cfg.el ends here
