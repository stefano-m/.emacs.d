;;; javascript.cfg.el --- Summary
;;; Commentary:
;;; JavaScript configuration
;;; Code:
(use-package js2-mode
  :defines js2-indent-switch-body
  :init
  (add-hook 'js2-mode-hook #'yas-minor-mode)
  (add-hook 'js2-mode-hook #'flycheck-mode)
  :config
  (setq js2-basic-offset 2)
  (setq js2-indent-switch-body t)
  :mode ("\\.js\\'" . js2-mode)
  ;; Requires nodejs to be in $PATH
  :interpreter ("node" . js2-mode))

(use-package json-mode
  :config

  ;; JSON-Schema macros
  (fset 'json-schema-insert-comment
        [?\" ?$ ?c ?o ?m ?m ?e ?n ?t right ?: ?  ?\"])

  (fset 'json-schema-insert-property
        [?\" right ?: ?  ?\{ return return up tab ?\" ?t ?y ?p ?e right ?: ?  ?\" right ?, return ?\" ?d ?e ?s ?c ?r ?i ?p ?t ?i ?o ?n right ?: ?  ?\" right ?, return ?\" ?$ ?c ?o ?m ?m ?e ?n ?t right ?: ?  ?\" up up up left left left left])

  (fset 'json-schema-insert-ref
        [?\" ?$ ?r ?e ?f right ?: ?  ?\" ?# ?d ?e ?f ?i ?n ?i ?t ?i ?o ?n ?s ?/])

  (add-hook 'json-mode-hook #'flycheck-mode)


  (define-derived-mode cfn-json-mode js-mode
    "CFN-JSON"
    "Simple mode to edit CloudFormation template in JSON format.
    See `https://www.emacswiki.org/emacs/CfnLint' for more
    information."
    (setq js-indent-level 2))

  (add-to-list 'magic-mode-alist
               '("\\({\n *\\)? *[\"']AWSTemplateFormatVersion" . cfn-json-mode))
  (add-hook 'cfn-json-mode-hook #'flycheck-mode)

  :mode
  ("\\.tfstate$" . json-mode) ;; tfstate is the extension of the Terraform database.
  ("\\.json$" . json-mode))

(use-package js-comint
  :bind (:map js-comint-mode-map
              ("C-c C-l" . helm-comint-input-ring)))

(provide 'javascript.cfg)
;;; javascript.cfg.el ends here
