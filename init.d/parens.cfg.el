;;; parens.cfg.el --- Summary
;;; Commentary:
;;; parentheses configuration
;;; Code:
(use-package rainbow-delimiters
  :config
  (defvar on-mode-hooks
  '(emacs-lisp-mode-hook python-mode-hook js2-mode-hook shell-mode-hook shell-script-mode-hook sh-mode cider-repl-mode-hook))
  (defun enable-rainbow-delimiters (mode-hook)
    "Enable rainbow delimiters in MODE-HOOK."
    (add-hook mode-hook #'rainbow-delimiters-mode))
  (mapc 'enable-rainbow-delimiters on-mode-hooks))

(use-package smartparens-config
  :functions sp-local-pair
  :init
  (add-hook 'minibuffer-setup-hook #'turn-on-smartparens-strict-mode)
  :config
  (show-smartparens-global-mode t)
  (smartparens-global-mode t)
  (sp-use-smartparens-bindings)
  (sp-local-pair 'minibuffer-inactive-mode "'" nil :actions nil)
  :delight smartparens-mode)

(provide 'parens.cfg)
;;; parens.cfg.el ends here
