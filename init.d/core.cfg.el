;;; core.cfg.el --- Summary
;;; Commentary:
;;; Core Emacs configuration.
;;; Code:
(require 'server)
(unless (server-running-p)
  (server-start))

(mapc (lambda (mode)
        (when (fboundp mode)
          (apply mode '(-1))))
        '(tool-bar-mode menu-bar-mode scroll-bar-mode))

(setq inhibit-startup-screen t)

(setq gc-cons-threshold 50000000 ;; Increase garbage collection cache.
      read-process-output-max (* 1024 1024) ;; Increase the amount of data which Emacs reads from the process (1MiB)
      initial-scratch-message "" ;; no message in the *scratch* buffer
      initial-major-mode 'emacs-lisp-mode)

;; Remap to minimize the chance of closing by mistake.
(defvar my-kill-emacs-binding "C-x C-'")
(global-set-key (kbd my-kill-emacs-binding) 'save-buffers-kill-emacs)
;; Map C-x C-x to do nothing but warn that the mapping has changed
(defun warn-mapping-changed ()
  "Warn that the kill Emacs command mapping is different."
  (interactive)
  (warn (concat "The kill emacs command has been remapped to " my-kill-emacs-binding)))
(global-set-key (kbd "C-x C-c") 'warn-mapping-changed)

(use-package url
  :config
  ;; Just do not share anything when making network requests.
  (setq url-privacy-level 'paranoid)
  (url-setup-privacy-info))

;; Keep the path settings of the remote account.
(use-package tramp
  :init
  (add-hook
   'find-file-hook
   #'(lambda ()
     (when (file-remote-p default-directory)
       ;; Write the file contents into a temporary file first, which is checked for correct checksum.
       (set (make-local-variable 'file-precious-flag) t))))
  :config
  (add-to-list 'tramp-remote-path 'tramp-own-remote-path)
  (advice-add 'tramp :before (lambda () (setenv "SHELL" "/bin/sh"))))

(add-hook 'text-mode-hook #'text-mode-hook-identify)

(add-to-list 'safe-local-variable-values '(TeX-master . "main"))

(use-package eldoc
  :delight eldoc-mode)

(require 'appearance.cfg)
(require 'gud.cfg)
(require 'buffer.cfg)

(require 'autocomplete.cfg)
(require 'helm.cfg)
(require 'project.cfg)
(require 'parens.cfg)

(use-package yasnippet
  :functions yas-reload-all
  :config
  (use-package yasnippet-snippets)
  (yas-global-mode 1)
  :delight yas-minor-mode)

(use-package flycheck
  :init
  (defun my/ansible-modep ()
    ;; for some reason use-package does not like having this has a lambda after
    ;; :predicate
    (and (boundp 'ansible) ansible))

  :config
  (flycheck-define-checker cfn-lint
    "AWS CloudFormation linter using cfn-lint.  See
`https://github.com/aws-cloudformation/cfn-python-lint'.  See
`https://www.emacswiki.org/emacs/CfnLint for more information'."
    :command ("cfn-lint" "-f" "parseable" source)
    :error-patterns ((warning line-start (file-name) ":" line ":" column
                              ":" (one-or-more digit) ":" (one-or-more digit) ":"
                              (id "W" (one-or-more digit)) ":" (message) line-end)
                     (error line-start (file-name) ":" line ":" column
                            ":" (one-or-more digit) ":" (one-or-more digit) ":"
                            (id "E" (one-or-more digit)) ":" (message) line-end))
    :modes (cfn-json-mode cfn-yaml-mode))
  (add-to-list 'flycheck-checkers 'cfn-lint)

  (flycheck-define-checker ansible-lint
    "Checker for Ansible roles and playbooks.  See
`https://ansible-lint.readthedocs.io' for more information."
    :command ("ansible-lint" "--nocolor" "-p" source)
    :error-patterns ((warning line-start (file-name) ":" line ":" (one-or-more space) "[" (id "W" (one-or-more digit)) "] " (message) line-end)
                     (error line-start (file-name) ":" line ":" (one-or-more space) "[" (id "E" (one-or-more digit)) "] " (message) line-end)
                     (error line-start "We were unable to read either as JSON nor YAML"))
    :predicate my/ansible-modep ;; use-package does not like (lambda () (...)) here
    :modes yaml-mode)
  (add-to-list 'flycheck-checkers 'ansible-lint)
  (flycheck-add-next-checker 'yaml-ruby '(error . ansible-lint))
  (flycheck-add-next-checker 'yaml-jsyaml '(error . ansible-lint)))

(require 'vcs.cfg)

(provide 'core.cfg)
;;; core.cfg.el ends here
