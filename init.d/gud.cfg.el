;;; gud.cfg.el --- Summary
;;; Commentary:
;;; grand-unified debugger configuration
;;; Code:

(use-package dap-mode
  :config
  (dap-mode t)
  (dap-ui-mode t)
  (tooltip-mode t))

(use-package gdb-mi
  ;; Technically speaking, this is *not* the GUD.
  :config
  (setq
   ;; use gdb-many-windows by default
   gdb-many-windows t
   ;; Non-nil means display source file containing the main routine at startup
   gdb-show-main t))

;; Highlight whole line in GUD.
;; http://www.emacswiki.org/emacs/DebuggingWithEmacs
(defvar gud-overlay
  (let* ((ov (make-overlay (point-min) (point-min))))
    ;; (overlay-put ov 'face 'secondary-selection)
    (overlay-put ov 'face 'region)
    ov)
  "Overlay variable for GUD highlighting.")

(use-package gud
  :functions my-gud-highlight gud-find-file
  :init
  (defun my-gud-highlight (true-file &optional line)
    "In TRUE-FILE, highlight current LINE (actually unused, but needed by the advice)."
    (let* ((ov gud-overlay)
           (bf (gud-find-file true-file)))
      (with-current-buffer bf
        (move-overlay ov (line-beginning-position) (line-beginning-position 2)
                      (current-buffer)))))
  :config
  (advice-add 'gud-display-line :after #'my-gud-highlight))

;;;###autoload
(defun gud-kill-buffer ()
  "Kill the gud buffer."
  (if (derived-mode-p 'gud-mode)
      (delete-overlay gud-overlay)))

(add-hook 'kill-buffer-hook #'gud-kill-buffer)

(provide 'gud.cfg)
;;; gud.cfg.el ends here
