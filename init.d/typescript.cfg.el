;;; typescript.cfg.el --- Summary
;;; Commentary:
;;; TypeScript configuration
;;; Code:

(use-package typescript-mode
  :config
  (use-package lsp-typescript
    :config
    (add-hook 'typescript-mode-hook #'lsp-typescript-enable))
  (add-hook 'before-save-hook #'lsp-format-buffer)
  (add-hook 'typescript-mode-hook #'flycheck-mode)
  (add-hook 'typescript-mode-hook #'yas-minor-mode)
  :delight (typescript-mode "ts")
  :mode
  (("\\.ts$" . typescript-mode)
   ("\\.d\\.ts$" . typescript-mode)
   ("\\.tsx$" . typescript-mode))
  :interpreter ("node" . typescript-mode))

(provide 'typescript.cfg)
;;; typescript.cfg.el ends here
