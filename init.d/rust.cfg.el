;;; rust.cfg.el --- Summary
;;; Commentary:
;;; Rust configuration
;;; Code:

(use-package rust-mode
  :defines rust-format-on-save
  :config
  (setq rust-format-on-save t)
  :init
  (add-hook 'rust-mode-hook #'(lambda () (setq indent-tabs-mode nil)))
  (add-hook 'rust-mode-hook #'lsp))

(use-package racer
  :hook (rust-mode . racer-mode)
  :init
  (add-hook 'racer-mode-hook #'eldoc-mode)
  (add-hook 'racer-mode-hook #'company-mode))

(use-package cargo
  :hook (rust-mode . cargo-minor-mode))

(provide 'rust.cfg)
;;; rust.cfg.el ends here
