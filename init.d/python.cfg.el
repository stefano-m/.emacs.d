;;; python.cfg.el --- Summary
;;; Commentary:
;;; python configuration
;;; Code:


(use-package python
  :defines gud-pdb-command-name
  :functions gud-query-cmdline my-prepare-pdb-args
  :hook
  (python-mode . lsp-deferred)
  (python-mode . hs-minor-mode)
  (python-mode . sphinx-doc-mode)
  :init
  (setq gud-pdb-command-name "python -m pdb")
  (defun my-prepare-pdb-args (minor-mode &optional init)
    "Modify MINOR-MODE and INIT args to be passed to `gud-query-cmdline'.
If MINOR-MODE is not equal to '(pdb) does nothing, otherwise pass
the pdb mode and the file name of the current buffer."
    (let ((fname (buffer-file-name (current-buffer)))
          (mode (car minor-mode)))
      (if (equal mode 'pdb)
          (list mode fname)
        (list mode init))))
  (advice-add 'gud-query-cmdline :filter-args #'my-prepare-pdb-args)
  (unless (bound-and-true-p lsp-prefer-flymake)
    (add-hook 'python-mode-hook #'flycheck-mode))
  :config
  (setq python-indent-guess-indent-offset nil)
  (with-temp-buffer
    (when (and (= 0 (call-process "python" nil t nil "-m" "ptvsd" "--version"))
               (version-list-<= '(4 2) (version-to-list (string-trim (buffer-string)))))
      (use-package dap-python)))
  (use-package sphinx-doc
    :bind (:map sphinx-doc-mode-map
                ("C-c M-d" . nil)
                ("C-c C-d" . sphinx-doc))
    :delight sphinx-doc-mode)

  (use-package virtualenvwrapper
    :config
    (progn
      (venv-initialize-interactive-shells)
      (venv-initialize-eshell)))
  :mode ("\\.py\\'" . python-mode)
  :interpreter ("python" . python-mode)
  :bind (:map inferior-python-mode-map
              ("C-c C-l" . helm-comint-input-ring)))

(provide 'python.cfg)
;;; python.cfg.el ends here
