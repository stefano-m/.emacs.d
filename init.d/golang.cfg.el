;;; golang.cfg.el --- Summary
;;; Commentary:
;;; Go configuration
;;; Code:
(use-package go-mode
  :hook
  (go-mode . flycheck-mode)
  (go-mode . lsp-deferred)
  (before-save . gofmt-before-save))

(provide 'golang.cfg)
;;; golang.cfg.el ends here
