;;; groovy.cfg.el --- Summary
;;; Commentary:
;;; groovy and related (gradle, Jenkinsfile, etc) configuration
;;; Code:

(use-package groovy-mode)
(use-package jenkinsfile-mode)

(provide 'groovy.cfg)
;;; groovy.cfg.el ends here
