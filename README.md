# My GNU Emacs Configuration

This is my GNU Emacs configuration, it is unlikely that anyone else will be
using it. If you are looking for a comprehensive configuration to get you
started you can look at larger projects
like [Spacemacs](http://spacemacs.org/).

This configuration supports a bunch of editing modes that I need for work or
pleasure. Not all modes are equally supported as I may be switching back and
forth. These editing modes are grouped in "user modules".

The configuration employs the
great [`use-package`](https://github.com/jwiegley/use-package/) package to
optimize load time.

There are a bunch of custom key bindings mostly in specific mode maps, for
example I try to always map commands that show the documentation to `M-?`. At
the moment the key bindings are not documented, but they can be easily found by
running `M-x describe-bindings` (usually mapped to `C-h b`).

## Supported GNU Emacs Version

This configuration is known to work with Emacs `27.2`.

## Installation

This project *should* be installed using [the `nix` package
manager](https://nixos.org/guides/how-nix-works.html). The [*experimental*
flake
feature](https://nixos.org/manual/nix/unstable/command-ref/new-cli/nix3-flake.html)
is also supported.

### Using `nix` stable

After checking out this project, you can build the [nix
derivation](https://nixos.org/manual/nix/stable/#ssec-derivation) with
[`nix-build`](https://nixos.org/manual/nix/stable/#sec-nix-build).

``` shell
nix-build -E 'with import <nixpkgs> { }; callPackage ./default.nix { }'
```

The derivation lets you build different flavours of Emacs based on the `extraConfigs` argument. For example:

``` shell
nix-build -E 'with import <nixpkgs> { }; callPackage ./default.nix { extraConfigs=["clojure"]; }'
```

For example, you could create a [nix
overlay](https://nixos.org/manual/nixpkgs/stable/#chap-overlays) to install
Emacs in your profile.

### Using the *experimental* flake feature (`nix` unstable)

This project leverages the [*experimental* nix flake
feature](https://nixos.org/manual/nix/unstable/command-ref/new-cli/nix3-flake.html).

#### Including into another flake

You can declare this flake as *input* to another flake and include its packages, overlays, etc.

``` nix
{
    description = "Some flake that uses my Emacs";
    inputs.my-emacs.url = "path to this project";

    outputs = { self, nixpkgs, my-emacs}: {
        # use the my-emacs flake here
    };
}
```

#### Building

You can use the [*experimental* `nix
build`](https://nixos.org/manual/nix/unstable/command-ref/new-cli/nix3-build.html).

``` shell
# build the default emacs
nix build

# add the Python configuration
nix build '#.python-ide'

# add the Lua configuration
nix build '#.lua-ide'

# build everything
nix build '#.full'
```

#### Running

You can also directly *run* the Emacs configuration using [`nix
run`](https://nixos.org/manual/nix/unstable/command-ref/new-cli/nix3-run.html)
rather than `nix build`.

## Further Configuration

You can use the usual [*Emacs init
file*](https://www.gnu.org/software/emacs/manual/html_node/emacs/Init-File.html)
for further configuration.

[Customizations made with
`customize`](https://www.gnu.org/software/emacs/manual/html_node/emacs/Customization.html),
are [saved in a file called
`private.el`](https://www.gnu.org/software/emacs/manual/html_node/emacs/Saving-Customizations.html). It's
a good idea to keep `private.el` versioned in an external repository and
symlink it to `~/.emacs.d`.

## A Note On Versioning

This configuration is loosely versioned. I try to tag whenever things seem
stable enough.
