;;; Commentary:
;;; Emacs good 'ole init file.
;;; Code:

(when (string= system-type 'darwin)
  ;; set keys for Apple keyboard, for emacs in OS X
  (setq mac-command-modifier 'meta ; make cmd key do Meta
        mac-option-modifier 'super ; make opt key do Super
        mac-control-modifier 'control) ; make Control key do Control
  (global-set-key [home] #'beginning-of-line)
  (global-set-key [end] #'end-of-line))

;; Load private stuff if present customization will be saved here.
(let ((private-file (concat user-emacs-directory "private.el")))
  (if (file-readable-p private-file)
      (progn
        (setq custom-file private-file)
        (load custom-file t))
    (message
     (format
      "Cannot load custom file %s (does it exist and is it readable?)"
      private-file))))

(let ((backup-dir (expand-file-name (concat user-emacs-directory "backups"))))
  (custom-set-variables '(make-backup-files nil)) ; disable backup files altogether
  (unless (file-directory-p backup-dir) (make-directory backup-dir))
  (setq backup-directory-alist
        `((,tramp-file-name-regexp . nil) ; exclude Tramp files
          ("COMMIT_EDITMSG" .  nil)
          ("." . ,backup-dir))))

(setq backup-by-copying t
      delete-old-versions t
      kept-new-versions 4
      kept-old-versions 2
      version-control t)

(setq load-prefer-newer t)

(require 'tls.cfg) ;; GNU Emacs has unsafe default TLS settings
(safer-tls/setup)

(custom-set-variables
 '(custom-safe-themes
   (quote
    (
     ;; solarized-wombat-dark
     "3e200d49451ec4b8baa068c989e7fba2a97646091fd555eca0ee5a1386d56077"
     ;; smart-mode-line sml/theme respectful
     "c74e83f8aa4c78a121b52146eadb792c9facc5b1f02c917e3dbb454fca931223"
     default))))

(load-theme 'solarized-wombat-dark)

(setq sml/theme 'respectful)
(sml/setup)

(eval-when-compile
  (require 'use-package))

(use-package enriched
  :config
  ;; Mitigate Bug#28350 (security) in Emacs 25.2 and earlier.
  ;; https://debbugs.gnu.org/cgi/bugreport.cgi?bug=28350
  (when (version< emacs-version "25.3")
    (advice-add 'enriched-decode-display-prop
                :override
                (lambda (start end &optional param)
                  (list start end)))))

;; Ensure correct indentation for use-package.
(put 'use-package 'lisp-indent-function 1)

(require 'delight)

(use-package delight
  :config
  (delight 'visual-line-mode))

(require 'bind-key) ;; to use :bind

(use-package auto-compile ;; TODO: remove this?
  :config
  (auto-compile-on-load-mode))

(require 'core.cfg)

(use-package elisp-mode
  :init
  (add-hook 'emacs-lisp-mode-hook #'yas-minor-mode)
  (add-hook 'emacs-lisp-mode-hook
            #'(lambda ()
                (unless (string-equal "*scratch*" (buffer-name))
                  flycheck-mode))))

(use-package tdd) ;; Vendored.

(use-package user-modules)

;;; default.el ends here
